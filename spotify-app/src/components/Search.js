import { useState, useEffect } from 'react'
import SpotifyWebApi from 'spotify-web-api-node'
import TrackSearchResult from './TrackSearchResult'

const spotifyApi = new SpotifyWebApi({
  clientId: 'fdae10466fa14216ad1800aa9ec009b3'
  
})

export default function Search ({ searchStatus, chooseTrack, accessToken }) {
  const [search, setSearch] = useState('')
  const [searchResults, setSearchResults] = useState([])

  useEffect(() => {
    if (!accessToken) return
    spotifyApi.setAccessToken(accessToken)
  }, [accessToken])

  useEffect(() => {
    if (!search) return setSearchResults([])
    if (!accessToken) return

    let cancel = false
    spotifyApi.searchTracks(search).then(res => {
      if (cancel) return
      setSearchResults(
        res.body.tracks.items.map(track => {
          const smallestAlbumImage = track.album.images.reduce(
            (smallest, image) => {
              if (image.height < smallest.height) return image
              return smallest
            },
            track.album.images[0]
          )

          return {
            artist: track.artists[0].name,
            title: track.name,
            uri: track.uri,
            albumUrl: smallestAlbumImage.url
          }
        })
      )
    })

    return () => (cancel = true)
  }, [search, accessToken])

  return (
    <div
      className={`flex w-96 fixed top-0 left-0 bottom-12 bg-slate-200 shadow-2xl shadow-slate-300 -translate-x-full transition-all duration-300 opacity-0 ${
        searchStatus ? 'translate-x-0 opacity-100' : ''
      }`}
    >
      <div className='flex-col container justify-center'>
        <div>
          <input
            id='search'
            value={search}
            onChange={e => setSearch(e.target.value)}
            type='text'
            placeholder='Search songs...'
            className='w-80 h-16 py-2 ml-2 bg-transparent text-xl capitalize border-b-4 border-gray-600 outline-none'
          />
        </div>
        <div className='flex-col mt-5 overflow-y-scroll max-h-screen'>
          {searchResults.map(track => (
            <TrackSearchResult
              track={track}
              key={track.uri}
              chooseTrack={chooseTrack}
            />
          ))}
        </div>
      </div>
    </div>
  )
}

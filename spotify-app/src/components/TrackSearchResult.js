import React from 'react'

export default function TrackSearchResult ({ track, chooseTrack }) {
  function handlePlay () {
    chooseTrack(track)
  }

  return (
    <div
      id={track.title}
      className='flex mt-2 ml-2 items-center hover:bg-gray-300 rounded transition-all '
      style={{ cursor: 'pointer' }}
      onClick={handlePlay}
    >
      <img src={track.albumUrl} style={{ height: '64px', width: '64px' }} />
      <div className='ml-3'>
        <div>{track.title}</div>
        <div className='text-gray-400'>{track.artist}</div>
      </div>
    </div>
  )
}

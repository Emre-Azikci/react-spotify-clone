import React from 'react'
import { useState } from 'react'
import Nav from './Nav'
import Search from './Search'
import Player from './Player'
import useAuth from '../useAuth'

export default function Dashboard () {
  const code = new URLSearchParams(window.location.search).get('code')
  const accessToken = useAuth(code)
  const [searchStatus, setSearchStatus] = useState(false)
  const [playingTrack, setPlayingTrack] = useState()

  function chooseTrack (track) {
    setPlayingTrack(track)
  }

  return (
    <div
      className={`transition-all duration-300 bg-gray-900 h-screen ${
        searchStatus ? 'ml-96' : ''
      }`}
      id='homepage'
    >
      <Nav searchStatus={searchStatus} setSearchStatus={setSearchStatus} />
      <Search
        searchStatus={searchStatus}
        chooseTrack={chooseTrack}
        accessToken={accessToken}
      />
      <div className='w-full fixed bottom-0 left-0'>
        <Player accessToken={accessToken} trackUri={playingTrack?.uri} />
      </div>
    </div>
  )
}

require('dotenv').config()
const puppeteer = require('puppeteer')

//create global variables to be used in the beforeAll function
let browser
let page

beforeAll(async () => {
  // launch browser
  browser = await puppeteer.launch({
    headless: false, // headless mode set to false so browser opens up with visual feedback
    defaultViewport: null, // removes the default Viewport of 800x600
    slowMo: 50 // how slow actions should be
  })
  // creates a new page in the opened browser
  page = await browser.newPage()
})

describe('Spotify', () => {
  test('Users can login', async () => {
    await page.goto('http://localhost:3000/')
    await page.waitForSelector('#login')
    await page.click('[id="loginButton"]')
    await page.waitForSelector('#root')
    await page.click('input[id="login-username"]')
    await page.type('input[id="login-username"]', process.env.USER_NAME)
    await page.click('input[id="login-password"]')
    await page.type('input[id="login-password"]', process.env.USER_PASSWORD)
    await page.click('button[id="login-button"]')
    await page.waitForSelector('[id="homepage"]')
  }, 1600000)

  test('Users can search for music', async () => {
    await page.goto('http://localhost:3000/')
    await page.waitForSelector('#login')

    await page.click('[id="loginButton"]')
    await page.waitForSelector('[id="homepage"]')

    await page.click('#searchBtn')
    await page.click('input[id="search"]')
    await page.type('input[id="search"]', 'ye')
  }, 1600000)

  test('Users can play music', async () => {
    await page.goto('http://localhost:3000/')
    await page.waitForSelector('#login')

    await page.click('[id="loginButton"]')
    await page.waitForSelector('[id="homepage"]')

    await page.click('#searchBtn')
    await page.click('input[id="search"]')
    await page.type('input[id="search"]', 'Ye')
    await page.click('#Ye')
  }, 1600000)
})

// This function occurs after the result of each tests, it closes the browser
afterAll(() => {
  browser.close()
})
